// [Section] Query Operators and Field Projection
// Looking for specific documents in our database

// [Section] Comparison Query Operators
/*
	allows us to find documents that have field number values greater than (or gte) or less than (or lte) to a specified value
	Syntax:
		db.collectionName.find({ field : { $gt/gte : value} });
		db.collectionName.find({ field : { $lt/lte : value} });
*/

// greater than operator
db.users.find({ age: { $gt : 50} });
db.users.find({ age: { $gte : 50} });

// less than operator
db.users.find({ age: { $lt : 50} });
db.users.find({ age: { $lte : 50} });

// $ne operator
/*
	allows us to find documents that have field number values not equal to a specified value

	Syntax:
		db.collectionName.find({ field : { $ne : value } })
*/
db.users.find({ age : { $ne : 82 } })

// $in operator
/*
	allows us to find documents with specific match criteria one field using different values

	Syntax:
		db.collectionName.find({ field : { $in : value } })
*/
// for single values
db.users.find({ lastName : { $in : ["Doe"] } });
// for multiple values
db.users.find({ firstName : { $in : ["Jane", "Stephen"] } });

// [Section] Logical Query Operators
// or operator
/*
	allows us to find documents that match a single criteria from multiple provided search criteria

	Syntax:
		db.collectionName.find({ $or : [ { fieldA : valueA }, { fieldB : valueB } ] });
*/
db.users.find({ $or : [ { firstName : "Neil" },  { age : 25 } ] });

// $and operator
/*
	allows us to find documents matching multiple criteria in a single field

	Syntax:
	db.collectionName.find({ $and: [ { FieldA : valueA }, { fieldB : valueB } ] });
*/
db.users.find({ $and: [ { age : { $ne : 82 } }, { age : { $ne : 76 } } ] });

// [Section] Field projection
/*
	-retrieving documents are common operations we do and by default mongoDB queries return the whole document
	as a response
	-there might be instances when fields are not useful for the query tht we are doing
	- we can try to include/exclude fields from the response
*/
// inclusion
/*
	-allows us to add specific fields only when retrieving documents
	- the value 1 is to denote that the field is being included
	-Syntax:
		db.users.find({criteria},{field:1});
*/
db.users.find(
	{firstName: "Jane"},
	{
		firstName: 1,
		lastName: 1,
		contact: 1
	}
);


// exclusion
/*
	-allows us to remove specific fields only when retrieving documents
	- the value 0 is to denote that the field is being excluded
	-Syntax:
		db.users.find({criteria},{field:0});
		
*/
db.users.find(
	{firstName: "Jane"},
	{
		contact: 0,
		department: 0
	}
);

// supressing the ID field
/*
	-in general, when using field projection, field inclusion and exclusion may not be used at the same time.
	-allows us to exclude "_id" field when retrieving documents
*/
db.users.find(
	{firstName: "Jane"},
	{
		_id:0,
		firstName: 1,
		lastName: 1
	}
);

// accessing fields inside embedded documents
db.users.find(
	{firstName: "Jane"},
	{
		firstName: 1,
		"contact.phone": 0    //if placed outside quotation marks, mongoDB reds them as two diff properties
	}
);

// Documents added to be used for slice
db.users.insert({
    namearr:[
    {
        namea: "juan"
    },
    {
        nameb: "tamed"
     }
    ]
});

// field projection for specific array elements
// $slice operator
db.users.find(
	{"namearr":
		{namea: "juan"}
	},
	{
		namearr:
			{$slice: 1}
	}
);

// [Section] Evaluation Querry Operator
// $regex operator - allows us to find documents that match a specific string pattern using regular expressions
/*
	Syntax:
		db.collectionName.find({ field : {$regex: 'pattern'} })
*/ 
// Case sensitive
db.users.find({firstName:{$regex: "N"}});
// Case insensitive
db.users.find({firstName:{$regex: "j", $options: "$i"}});